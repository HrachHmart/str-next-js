module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ['en', 'de'],
    defaultLocale: 'en',
  },
  images: {
    domains: ['static.coinstats.app'],
  },
  webpack: (config, { defaultLoaders }) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: [
        {
          loader: 'sass-resources-loader',
          options: {
            resources: ['./src/assets/styles/index.scss'],
          },
        },
      ],
    });

    return config;
  },
};
