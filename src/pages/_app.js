import React, { useMemo } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { IntlProvider } from 'react-intl';

import '../assets/styles/reset.scss';
import '../assets/styles/pages/global.page.scss';

import '../components/tableRows/coin.tableRow.scss';

import English from '../../content/compiled-locales/en.json';
import German from '../../content/compiled-locales/de.json';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const { locale, pathname, query } = router;

  const [shortLocale] = locale ? locale.split('-') : ['en'];

  const messages = useMemo(() => {
    switch (shortLocale) {
      case 'en':
        return English;
      case 'de':
        return German;
      default:
        return English;
    }
  }, [shortLocale]);

  return (
    <IntlProvider locale={shortLocale} messages={messages} onError={() => null}>
      <div className="lang_switcher_wrapper">
        <ul className="lang_switcher">
          <li>
            <Link
              href={{
                pathname,
                query,
              }}
              locale="en"
            >
              <a>English</a>
            </Link>
          </li>
          <li>
            <Link
              href={{
                pathname,
                query,
              }}
              locale="de"
            >
              <a>German</a>
            </Link>
          </li>
        </ul>
      </div>
      <Component {...pageProps} />
    </IntlProvider>
  );
}

export default MyApp;
