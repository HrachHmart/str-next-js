import React, { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { isClient } from '../../helpers/detector.helper';
import { getCoin_req } from '../../api/coins.api';
import CoinTableRow from '../../components/tableRows/coin.tableRow';
import CoinTableColumn from '../../components/tableColumns/coin.tableColumn';

export default function CoinPage({ coin }) {
  const router = useRouter();
  const { coinId } = router.query;
  const [coinData, setCoinData] = useState(coin);

  const getCoin = useCallback(async () => {
    try {
      const response = await getCoin_req({
        coinId,
      });
      if (response && response.coin) {
        setCoinData(response.coin);
      }
    } catch (e) {
      console.log('CoinPage -> getCoins_req Error: ', e);
    }
  }, [coinId]);

  useEffect(() => {
    if (!coinId) {
      return router.push('/', '/');
    }

    if (!coinData) {
      getCoin();
    }

    return () => {};
  }, []);

  if (!coinData) {
    return <div className="page" />;
  }

  return (
    <div className="page">
      <h1>{coinData.name}</h1>
      <table cellPadding="0" cellSpacing="0" border="0" className="coins_table">
        <thead>
          <CoinTableColumn />
        </thead>
        <tbody>
          <CoinTableRow coin={coinData} />
        </tbody>
      </table>
    </div>
  );
}

CoinPage.getInitialProps = async function ({ query }) {
  let coin = null;
  const { coinId } = query;

  if (!isClient() && coinId) {
    try {
      const response = await getCoin_req({
        coinId,
      });
      if (response.coin) {
        coin = response.coin;
      }
    } catch (e) {
      console.log('CoinPage.getInitialProps -> getCoins_req Error: ', e);
    }
  }

  return {
    coin,
  };
};
