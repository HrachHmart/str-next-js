import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return {
      ...initialProps,
      pathname: ctx.pathname,
      locales: ctx.locales,
      asPath: ctx.asPath,
      query: ctx.query,
    };
  }

  getAlternates() {
    const { locales = [], asPath } = this.props;

    const alternates = locales.map((locale) => (
      <link
        rel="alternate"
        href={`http://localhost:3000/${locale}${asPath}`}
        hrefLang={locale}
        key={locale}
      />
    ));

    alternates.push(
      <link
        rel="alternate"
        href={`http://localhost:3000${asPath}`}
        hrefLang="x-default"
        key="default"
      />
    );

    return alternates;
  }

  render() {
    this.getAlternates();
    const { locale } = this.props;

    return (
      <Html lang={locale}>
        <Head>
          <meta charSet="utf-8" />
          <meta name="msapplication-TileColor" content="#1C2630" />
          <meta name="theme-color" content="#293746" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link rel="manifest" href="/manifest.json" />
          <link
            rel="apple-touch-icon"
            sizes="180x180"
            href="/apple-touch-icon.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="32x32"
            href="/favicon-32x32.png"
          />
          <link
            rel="icon"
            type="image/png"
            sizes="16x16"
            href="/favicon-16x16.png"
          />
          <link rel="manifest" href="/site.webmanifest" />
          {this.getAlternates()}
          <title>Next JS</title>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
