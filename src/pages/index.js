import React, { useCallback, useEffect, useState } from 'react';

import { isClient } from '../helpers/detector.helper';
import { getCoins_req } from '../api/coins.api';
import CoinTableRow from '../components/tableRows/coin.tableRow';
import CoinTableColumn from '../components/tableColumns/coin.tableColumn';

export default function Home({ coins }) {
  const [coinsData, setCoinsData] = useState(coins);

  const getCoins = useCallback(async () => {
    try {
      const response = await getCoins_req({
        skip: 0,
        limit: 20,
        currency: 'USD',
      });
      if (response && response.coins) {
        setCoinsData(response.coins);
      }
    } catch (e) {
      console.log('Home -> getCoins_req Error: ', e);
    }
  }, []);

  useEffect(() => {
    if (!coins) {
      getCoins();
    }
  }, []);

  return (
    <div id="home-page" className="page">
      <main>
        <h1>Coins</h1>
        <table
          cellPadding="0"
          cellSpacing="0"
          border="0"
          className="coins_table"
        >
          <thead>
            <CoinTableColumn mini />
          </thead>
          <tbody>
            {coinsData && coinsData.length ? (
              coinsData.map((coin) => (
                <CoinTableRow key={coin.name} coin={coin} mini />
              ))
            ) : (
              <></>
            )}
          </tbody>
        </table>
      </main>
      <footer />
    </div>
  );
}

Home.getInitialProps = async function () {
  let coins = null;
  if (!isClient()) {
    try {
      const response = await getCoins_req({
        skip: 0,
        limit: 20,
        currency: 'USD',
      });
      if (response && response.coins) {
        coins = response.coins;
      }
    } catch (e) {
      console.log('Home.getInitialProps -> getCoins_req Error: ', e);
    }
  }

  return {
    coins,
  };
};
