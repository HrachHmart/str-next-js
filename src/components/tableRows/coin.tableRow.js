import React, { useCallback } from 'react';
import Image from 'next/image';
import { useRouter } from 'next/router';

export default function CoinTableRow({ coin, mini }) {
  const router = useRouter();

  const goToCoinPage = useCallback(() => {
    router.push(`/coin/${coin.id}`, `/coin/${coin.id}`, {
      locale: router.locale,
    });
  }, [coin]);

  return (
    <tr className="coin_table_row" key={coin.id} onClick={goToCoinPage}>
      <td>{coin.rank}</td>
      <td>{coin.name}</td>
      <td>
        <Image
          className="coin_image"
          src={coin.icon}
          layout="fixed"
          width={30}
          height={30}
        />
      </td>
      <td>{coin.price.toFixed(2)}</td>
      <td>{coin.priceBtc.toFixed(4)}</td>
      {!mini && (
        <>
          <td>{coin.priceChange1h}</td>
          <td>{coin.priceChange1d}</td>
          <td>{coin.priceChange1w}</td>
          <td>{coin.marketCap}</td>
        </>
      )}
    </tr>
  );
}
