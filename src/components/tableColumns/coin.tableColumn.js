import React from 'react';
import { FormattedMessage } from 'react-intl';

export default function CoinTableColumn({ mini }) {
  return (
    <tr>
      <th>
        <FormattedMessage id="rank" />
      </th>
      <th>
        <FormattedMessage id="name" />
      </th>
      <th />
      <th>
        <FormattedMessage id="price" />
      </th>
      <th>
        <FormattedMessage id="price_btc" />
      </th>
      {!mini && (
        <>
          <th>
            <FormattedMessage id="price_change_1h" />
          </th>
          <th>
            <FormattedMessage id="price_change_1d" />
          </th>
          <th>
            <FormattedMessage id="price_change_1w" />
          </th>
          <th>
            <FormattedMessage id="market_cap" />
          </th>
        </>
      )}
    </tr>
  );
}
