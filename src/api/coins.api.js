import { axiosInstanceV1 } from './axiosInstance';
import { parseResponse } from './helper';

const BASE = '/coins';

export function getCoins_req({ skip, limit, currency }) {
  return parseResponse(
    axiosInstanceV1.get(BASE, {
      params: {
        skip,
        limit,
        currency,
      },
    })
  );
}

export function getCoin_req({ coinId }) {
  return parseResponse(axiosInstanceV1.get(`${BASE}/${coinId}`));
}
