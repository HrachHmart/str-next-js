// eslint-disable-next-line import/prefer-default-export
export async function parseResponse(requestPromise) {
  const response = await requestPromise;
  return response.data;
}
